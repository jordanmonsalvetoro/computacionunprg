/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.unprg.app.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jorda
 */
@Entity
@Table(name="categoria")
public class CategoryEntity {
    @Id //Indica q se esta agregando el Id como clave primaria
    @GeneratedValue(strategy=GenerationType.IDENTITY) //Sirve para generar un autoincrementable
    @Column(name="id_Categoria") //@Column se establece cuando quieres poner el nombre de tu ID 
            //tu mismo y para establecer los nombre y dimenciones, etc de los otros campos 
    private Integer id;
    
    @Column(name="code", length = 10)
    private String Code;
    
    @Column(name="name", length=100)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
