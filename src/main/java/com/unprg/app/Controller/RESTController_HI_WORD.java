/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.unprg.app.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jorda
 */
@RestController
@RequestMapping("/Hi_World")
public class RESTController_HI_WORD {

    @GetMapping("")
    public String Saludo() {
        return "Hola mundo!/HI WORLD!";
    }

}
