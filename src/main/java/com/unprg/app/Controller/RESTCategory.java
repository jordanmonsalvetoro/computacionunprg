/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.unprg.app.Controller;

import com.unprg.app.type.Category;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jorda
 */
@RestController
@RequestMapping("/Category")
public class RESTCategory {

    public static List<Category> LCategoria=new ArrayList<>();

    public RESTCategory() {
        Category Categoria1 = new Category("1", "CAT1");
        Category Categoria2 = new Category("2", "CAT2");
        Category Categoria3 = new Category("3", "CAT3");
        Category Categoria4 = new Category("4", "CAT4");
        LCategoria.add(Categoria4);
        LCategoria.add(Categoria1);
        LCategoria.add(Categoria2);
        LCategoria.add(Categoria3);

    }

    @GetMapping("Lista")
    public List<Category> LCategory() {
        return LCategoria;
    }
}
